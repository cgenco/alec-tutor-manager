require 'test_helper'

class TutorsControllerTest < ActionController::TestCase
  setup do
    @tutor = tutors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tutors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tutor" do
    assert_difference('Tutor.count') do
      post :create, tutor: { capacity: @tutor.capacity, currently_tutoring: @tutor.currently_tutoring, email: @tutor.email, name: @tutor.name, room: @tutor.room, schedule_end: @tutor.schedule_end, schedule_start: @tutor.schedule_start }
    end

    assert_redirected_to tutor_path(assigns(:tutor))
  end

  test "should show tutor" do
    get :show, id: @tutor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tutor
    assert_response :success
  end

  test "should update tutor" do
    put :update, id: @tutor, tutor: { capacity: @tutor.capacity, currently_tutoring: @tutor.currently_tutoring, email: @tutor.email, name: @tutor.name, room: @tutor.room, schedule_end: @tutor.schedule_end, schedule_start: @tutor.schedule_start }
    assert_redirected_to tutor_path(assigns(:tutor))
  end

  test "should destroy tutor" do
    assert_difference('Tutor.count', -1) do
      delete :destroy, id: @tutor
    end

    assert_redirected_to tutors_path
  end
end
