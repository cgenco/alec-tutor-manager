# Functional Acceptance Tests

For the ALEC Tutor manager. For questions or comments, email cgenco@smu.edu.

## Feature: Look up tutor names for a course
  In order to direct students to an available tutor
  As an ALEC manager or administrator
  I should be able to look up the name of an available tutor for a subject

### Scenario: Manager should be able to log in                                     
    Given a manager account with email "manager@smu.edu" and password "password" 
    When I log in with email "manager@smu.edu" and password "password"           
    Then I should not see You need to sign in or sign up before continuing       
    And I should be on the home page                                             

**Pass**/**Fail**

### Scenario: Manager should not be able to log in with the wrong password         
    Given a manager account with email "manager@smu.edu" and password "password" 
    When I log in with email "manager@smu.edu" and password "wrong password"     
    Then I should see Invalid email or password.                                 
    And I should be on the login page                                            

**Pass**/**Fail**

### Scenario: Manager should be able to view tutor information on the home page                                                                
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named George Washington who tutors on Mon 2-3pm, Wed 3-4pm 
    When I log in as a manager                                                                                                               
    And I go to the home page                                                                                                                
    Then I should see George Washington                                                                                                      
    And I should see Mon 2-3pm                                                                                                               
    And I should see Wed 3-4pm                                                                                                               

**Pass**/**Fail**

### Scenario: Manager should be able to view tutor information on a course's page                                                              
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named George Washington who tutors on Mon 2-3pm, Wed 3-4pm 
    When I log in as a manager                                                                                                               
    And I go to the home page                                                                                                                
    And I click on Introduction To Financial Accounting                                                                                      
    Then I should see George Washington                                                                                                      
    And I should see Mon 2-3pm                                                                                                               
    And I should see Wed 3-4pm                                                                                 

**Pass**/**Fail**

## Feature: Students shouldn't be able to access certain pages
  In order to protect the privacy of tutors
  As an SMU Student
  I should not be able to see when a specific tutor is working or view tutor reviews


### Scenario: Students should not be able to access the /tutors page     
    When I go to the tutors page                                       
    Then I should see You need to sign in or sign up before continuing 
    And I should be on the login page                                  

**Pass**/**Fail**

### Scenario: Students should not be able to access the /reviews page    
    When I go to the reviews page                                      
    Then I should see You need to sign in or sign up before continuing 
    And I should be on the login page                                  

**Pass**/**Fail**

### Scenario: Students should not be able to view tutor information on the home page                                                           
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named George Washington who tutors on Mon 2-3pm, Wed 3-4pm 
    When I go to the home page                                                                                                               
    Then I should not see George Washington                                                                                                  

**Pass**/**Fail**

### Scenario: Students should not be able to view tutor information on a course's page                                                         
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named George Washington who tutors on Mon 2-3pm, Wed 3-4pm 
    When I go to the home page                                                                                                               
    And I click on Introduction To Financial Accounting                                                                                      
    Then I should not see George Washington                                                                                                  
**Pass**/**Fail**

## Feature: Check when a course will be tutored
  In order to not waste time going to the ALEC when a tutor for my subject is not available
  As an SMU Student
  I want to check when the next available tutor will be tutoring for a specific course number


### Scenario: Find the next available tutor for a course by course number                       
    Given the course CSE 1341 with a tutor named Joe Blogs who tutors on Mon 2-3pm, Wed 3-4pm 
    When I go to the home page                                                                
    And I click on CSE 1341                                                                   
    Then I should see Mon 2-3pm                                                               
    And I should see Wed 3-4pm                                                                

**Pass**/**Fail**

### Scenario: Find the next available tutor for a course by course name                                                                  
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named Paul Newman who tutors on Mon 2-3pm, Wed 3-4pm 
    When I go to the home page                                                                                                         
    And I click on Introduction To Financial Accounting                                                                                
    Then I should see Mon 2-3pm                                                                                                        
    And I should see Wed 3-4pm                                                                                                         
**Pass**/**Fail**

## Feature: Times for multiple tutors should combine logically
  In order to consolidate information from multiple tutors
  As the ALEC Tutor Manager
  I should combine multiple tutor's times and display them in the least amount of timespans possible


### Scenario: A course with tutors who have overlapping times should display the sum of their times 
    Given I have a tutor named Michael Jackson who tutors CSE 1341 on Mon 3-4pm                   
    And I have a tutor named Elvis Presley who tutors CSE 1341 on Mon 4-5pm                       
    When I log in as a manager                                                                    
    And I go to the home page                                                                     
    Then I should see Mon 3-5pm                                                                   
    And I should not see Mon 3-4pm                                                                
    And I should not see Mon 4-5pm                                                                

**Pass**/**Fail**

### Scenario: A course with tutors who have non-overlapping times should display the sum of their times 
    Given I have a tutor named Michael Jackson who tutors CSE 1341 on Mon 3-4pm                       
    And I have a tutor named Elvis Presley who tutors CSE 1341 on Mon 4:30-5pm                        
    When I log in as a manager                                                                        
    And I go to the home page                                                                         
    Then I should see 3-4pm                                                                           
    And I should see 4:30-5pm                                                                         
    And I should not see 3-5pm                                                                        

**Pass**/**Fail**

## Feature: Tutor checkin
  In order to keep track of where tutors are
  As an ALEC manager or administrator
  I should be able to check tutors in and out and change their room and status


### Scenario: Manager should be able to check a tutor in                                              
    Given I log in as a manager                                                                     
    And I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm 
    And I go to the tutors page                                                                     
    And I check Michael Jackson in                                                                  
      TODO (Cucumber::Pending)
      ./features/step_definitions/tutor_steps.rb:2:in `/^I check (.*) in$/'
      features/tutor_checkin.## feature:10:in `And I check Michael Jackson in'
    Then Michael Jackson should be clocked in                                                       

**Pass**/**Fail**

### Scenario: Manager should be able to check a tutor out                                             
    Given I log in as a manager                                                                     
    And I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm 
    And I go to the tutors page                                                                     
    And I check Michael Jackson in                                                                  
      TODO (Cucumber::Pending)
      ./features/step_definitions/tutor_steps.rb:2:in `/^I check (.*) in$/'
      features/tutor_checkin.## feature:17:in `And I check Michael Jackson in'
    And I check Michael Jackson out                                                                 
    Then Michael Jackson should be clocked out                                                      

**Pass**/**Fail**

### Scenario: Manager should be able to change a tutor's room                                         
    Given I log in as a manager                                                                     
    And I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm 
    And I go to the tutors page                                                                     
    And I set Michael Jackson's room to 202T                                                        
      TODO (Cucumber::Pending)
      ./features/step_definitions/tutor_steps.rb:18:in `/^I set (.*)'s room to (.*)$/'
      features/tutor_checkin.## feature:25:in `And I set Michael Jackson's room to 202T'
    And I check Michael Jackson in                                                                  
    Then Michael Jackson should be clocked in                                                       
    And Michael Jackson's room should be 202T                                                       

**Pass**/**Fail**

### Scenario: Manager should be able to change a tutor's status                                       
    Given I log in as a manager                                                                     
    And I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm 
    And I go to the tutors page                                                                     
    And I set Michael Jackson's status to "super busy"                                              
      TODO (Cucumber::Pending)
      ./features/step_definitions/tutor_steps.rb:26:in `/^I set (.*)'s status to "(.*)"$/'
      features/tutor_checkin.## feature:34:in `And I set Michael Jackson's status to "super busy"'
    Then Michael Jackson's status should be "super busy"                                            

**Pass**/**Fail**

## Feature: Automatically generate the page at smu.edu/alec/tutorsched.asp
  In order to save time for the ALEC Administrators who usually manually parse the data in the master excel file
  As the ALEC Tutor Manager
  I should automatically generate HTML code that can be cut and pasted to smu.edu/alec/tutorsched.asp

#: Check if page generates                                                                                               
    Given I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm      
    And I have a tutor named Kanye West who tutors CSE 1341, ECON 1401, EMIS 1334 on Thu 12-8pm, Fri 5-9pm 
    When I log in as a manager                                                                             
    And I go to the tutorsched page                                                                        
    Then I should see CSE 1341: Mon 3-4pm, Tue 5-7pm, Thu 12-8pm, Fri 5-9pm                                
    And I should see CSE:                                                                                  
    And I should see BIOL:                                                                                 
    And I should see ECON:                                                                                 
    And I should see EMIS:                                                                                 

**Pass**/**Fail**

## Feature: Upload data from Excel
  In order to reduce the change in the ALEC's current workflow
  As the ALEC Tutor Manager
  I should be able to import data from the current format that the ALEC stores their tutor data in

### Scenario: Uploaded excel data is reflected in the site 
    Given I log in as a manager                          
    And I go to the tutors page                          
    And I upload sample_tutor_schedule.xls               
    Then I should see George Washington                  
    And I should see Thomas Jefferson                    
    And I should see John Adams                          
    And I should see PLSC ALL                            
    And I should have 34 courses                         

# Nonfunctional Acceptance Tests

1. Each real-world analogous task with the functional acceptance tests will be done on the paper system and this electronic system by the same person several times. each trial will be timed, and the results averaged. the alec tutor manager system should, on average, be faster than the paper system. **Pass**/**Fail**
* Each real-world analogous task with the functional acceptance tests will be done on the paper system and this electronic system by several people. Each person will be asked which system they thought was easier, and will be prompted to quantify how much easier. The ALEC Tutor Manager system should, on average, be easier to use than the paper system. **Pass**/**Fail**
* All major functionality of the site should work on Google Chrome, Safari, Firefox, and Internet Explorer 9. **Pass**/**Fail**
* All major functionality of the site should work on an Apple iPhone 4, an Apple iPhone 5, an Apple iPad, an Android smartphone, and an Android tablet. **Pass**/**Fail**
* The site will should tested from internet connections both on and off campus to ensure equal usability in both circumstances. **Pass**/**Fail**
* Without access to valid login credentials, when the site is attempted to be compromised, the system should prevent the attacker from gaining entry. **Pass**/**Fail**
* While going through these acceptance tests, the site should not become unusable. **Pass**/**Fail**
* Simulate 100 simultaneous connections to the site by loading 10 different pages on 10 computers at the same time. Alternatively, use an online service like Blitz.io. **Pass**/**Fail**