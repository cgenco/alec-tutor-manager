require 'spec_helper'

describe Timespan do
  before do
    @t1 = Timespan.new(start_time: DateTime.parse("12pm"), stop_time: DateTime.parse("2pm"))
    @t2 = Timespan.new(start_time: DateTime.parse("1pm"), stop_time: DateTime.parse("2pm"))
    @t3 = Timespan.new(start_time: DateTime.parse("1pm"), stop_time: DateTime.parse("4pm"))
    @t4 = Timespan.new(start_time: DateTime.parse("5pm"), stop_time: DateTime.parse("8pm"))
  end

  it "should equal itself" do
    @t1.should eq @t1
  end

  specify "overlapping times should add together" do
    t = Timespan.new(start_time: DateTime.parse("12pm"), stop_time: DateTime.parse("4pm"))
    (@t1 + @t3).should eq [t]
  end

  specify "times should be sorted based on when they start" do
    [@t2, @t1].sort.should eq [@t1, @t2]
  end

  specify "two times reduced should be the same as two times added" do
    Timespan.reduce([@t1, @t2]).should eq (@t1 + @t2)
  end

  specify "reduced times should produce the same result in any order" do
    Timespan.reduce([@t1, @t2, @t3]).should eq Timespan.reduce([@t3, @t2, @t1])
  end

  specify "overlapping timespans should consolidate to one timespan" do
    t = Timespan.new(start_time: DateTime.parse("12pm"), stop_time: DateTime.parse("4pm"))
    Timespan.reduce([@t1, @t2, @t3]).should eq [t]
  end

  specify "non-overlapping timespans should be the minimum number of timespans necessary to describe the spans" do
    a = Timespan.new(start_time: DateTime.parse("12pm"), stop_time: DateTime.parse("4pm"))
    b = Timespan.new(start_time: DateTime.parse("5pm"), stop_time: DateTime.parse("8pm"))
    Timespan.reduce([@t1, @t2, @t3, @t4]).should eq [a, b]
  end
end
