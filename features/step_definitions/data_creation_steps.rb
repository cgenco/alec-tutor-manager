Given(/^I have a tutor named (.+) who tutors (.+) on (.+)$/) do |name, course_numbers, times|
  Tutor.create!(name: name, course_numbers: course_numbers.split(','), timespans: times.split(','))
end

Given(/^a manager account with email "(.*?)" and password "(.*?)"$/) do |email, password|
  User.create!(email: email, password: password)
end

Given(/^the course (.*) with a tutor named (.*) who tutors on (.*)$/) do |course_str, tutor_name, times|
  course_number, name = course_str.split(":")
  course = Course.create!(name: name, course_number: course_number)

  t = Tutor.create!(name: tutor_name, course_numbers: [course.course_number], timespans: times.split(','))
end
