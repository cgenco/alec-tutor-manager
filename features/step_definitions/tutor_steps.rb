Given(/^I check (.*) in$/) do |tutor_name|
  pending # express the regexp above with the code you wish you had
end

Then(/^(.*) should be clocked in$/) do |tutor_name|
  Tutor.find_by_name(tutor_name).clocked_in.should == true
end

Given(/^I check (.*) out$/) do |tutor_name|
  pending # express the regexp above with the code you wish you had
end

Then(/^(.*) should be clocked out$/) do |tutor_name|
  Tutor.find_by_name(tutor_name).clocked_in.should == false
end

Given(/^I set (.*)'s room to (.*)$/) do |tutor_name, room|
  pending # express the regexp above with the code you wish you had
end

Then(/^(.*)'s room should be (.*)$/) do |tutor_name, room|
  Tutor.find_by_name(tutor_name).room.should == room
end

Given(/^I set (.*)'s status to "(.*)"$/) do |tutor_name, status|
  pending # express the regexp above with the code you wish you had
end

Then(/^(.*)'s status should be "(.*)"$/) do |tutor_name, status|
  Tutor.find_by_name(tutor_name).status.should == status
end
