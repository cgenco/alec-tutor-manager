# from http://stackoverflow.com/questions/918066/reuse-cucumber-steps
module KnowsUser
  def login(email, password)
    visit('/users/sign_in')
    fill_in('user_email', :with => email)
    fill_in('user_password', :with => password)
    click_button('Sign in')
  end

  def create_user(email, password)
    u = User.find_by_email(email) || User.create!(:email => email, :password => password)
  end
end
World(KnowsUser)

When(/^I log in with email "(.*?)" and password "(.*?)"$/) do |email, password|
  create_user(email, password)
  login(email, password)
end

When(/^I log in as a manager$/) do
  email = "manager@smu.edu"
  password = "password"
  create_user(email, password)
  login(email, password)
end

When(/^I should have (\d+) courses/) do |count|
  Course.count.should == count.to_i
end