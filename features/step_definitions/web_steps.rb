When(/^I click on (.*)$/) do |link_name|
  # binding.pry
  find("a:contains(\"#{link_name}\")").click
  # click_link(link_name)
end

Then(/^I should see (.*)$/) do |content|
  page.should have_content(content)
end

Then(/^I should not see (.*)$/) do |content|
  page.should_not have_content(content)
end

When(/^I upload (.*)$/) do |filename|
  attach_file(:file, File.join(Rails.root, 'features', 'uploads', filename))
  click_button "Upload .csv or .xlxs"
end

# this isn't how I'm supposed to do this, but it works
module Pages
  def pages
    {
      'home' => '/',
      'login' => '/users/sign_in',
      'signin' => '/users/sign_in',
      'tutors' => '/tutors',
      'courses' => '/courses',
      'reviews' => '/reviews',
      'tutorsched' => '/courses/tutorsched'
    }
  end
end

World(Pages)

When(/^I go to the (.*) page$/) do |page_name|
  visit(pages[page_name])
end

Then(/^I should be on the (.*) page$/) do |page_name|
  page.current_path.should eq pages[page_name]
end
