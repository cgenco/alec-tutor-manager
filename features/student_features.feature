Feature: Check when a course will be tutored
  In order to not waste time going to the ALEC when a tutor for my subject is not available
  As an SMU Student
  I want to check when the next available tutor will be tutoring for a specific course number

  Scenario: Find the next available tutor for a course by course number
    Given the course CSE 1341 with a tutor named Joe Blogs who tutors on Mon 2-3pm, Wed 3-4pm
    When I go to the home page
    And I click on CSE 1341
    Then I should see Mon 2-3pm
    And I should see Wed 3-4pm
  
  Scenario: Find the next available tutor for a course by course name
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named Paul Newman who tutors on Mon 2-3pm, Wed 3-4pm
    When I go to the home page
    And I click on Introduction To Financial Accounting
    Then I should see Mon 2-3pm
    And I should see Wed 3-4pm