Feature: Upload data from Excel
  In order to reduce the change in the ALEC's current workflow
  As the ALEC Tutor Manager
  I should be able to import data from the current format that the ALEC stores their tutor data in

  Scenario: Uploaded excel data is reflected in the site
    Given I log in as a manager
    And I go to the tutors page
    And I upload sample_tutor_schedule.xls
    Then I should see George Washington
    And I should see Thomas Jefferson
    And I should see John Adams
    And I should see PLSC ALL
    And I should have 34 courses