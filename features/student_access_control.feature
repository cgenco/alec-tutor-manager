Feature: Students shouldn't be able to access certain pages
  In order to protect the privacy of tutors
  As an SMU Student
  I should not be able to see when a specific tutor is working or view tutor reviews

  Scenario: Students should not be able to access the /tutors page
    When I go to the tutors page
    Then I should see You need to sign in or sign up before continuing
    And I should be on the login page

  Scenario: Students should not be able to access the /reviews page
    When I go to the reviews page
    Then I should see You need to sign in or sign up before continuing
    And I should be on the login page

  Scenario: Students should not be able to view tutor information on the home page
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named George Washington who tutors on Mon 2-3pm, Wed 3-4pm
    When I go to the home page
    Then I should not see George Washington

  Scenario: Students should not be able to view tutor information on a course's page
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named George Washington who tutors on Mon 2-3pm, Wed 3-4pm
    When I go to the home page
    And I click on Introduction To Financial Accounting
    Then I should not see George Washington
