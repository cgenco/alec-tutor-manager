Feature: Tutor checkin
  In order to keep track of where tutors are
  As an ALEC manager or administrator
  I should be able to check tutors in and out and change their room and status

  Scenario: Manager should be able to check a tutor in
    Given I log in as a manager
    And I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm
    And I go to the tutors page
    And I check Michael Jackson in
    Then Michael Jackson should be clocked in

  Scenario: Manager should be able to check a tutor out
    Given I log in as a manager
    And I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm
    And I go to the tutors page
    And I check Michael Jackson in
    And I check Michael Jackson out
    Then Michael Jackson should be clocked out

  Scenario: Manager should be able to change a tutor's room
    Given I log in as a manager
    And I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm
    And I go to the tutors page
    And I set Michael Jackson's room to 202T
    And I check Michael Jackson in
    Then Michael Jackson should be clocked in
    And Michael Jackson's room should be 202T

  Scenario: Manager should be able to change a tutor's status
    Given I log in as a manager
    And I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm
    And I go to the tutors page
    And I set Michael Jackson's status to "super busy"
    Then Michael Jackson's status should be "super busy"