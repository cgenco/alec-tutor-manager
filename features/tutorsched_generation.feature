Feature: Automatically generate the page at smu.edu/alec/tutorsched.asp
  In order to save time for the ALEC Administrators who usually manually parse the data in the master excel file
  As the ALEC Tutor Manager
  I should automatically generate HTML code that can be cut and pasted to smu.edu/alec/tutorsched.asp

  Scenario: 
    Given I have a tutor named Michael Jackson who tutors CSE 1341, BIOL 1401 on Mon 3-4pm, Tue 5-7pm
    And I have a tutor named Kanye West who tutors CSE 1341, ECON 1401, EMIS 1334 on Thu 12-8pm, Fri 5-9pm
    When I log in as a manager
    And I go to the tutorsched page
    Then I should see CSE 1341: Mon 3-4pm, Tue 5-7pm, Thu 12-8pm, Fri 5-9pm
    And I should see CSE:
    And I should see BIOL:
    And I should see ECON:
    And I should see EMIS: