Feature: Times for multiple tutors should combine logically
  In order to consolidate information from multiple tutors
  As the ALEC Tutor Manager
  I should combine multiple tutor's times and display them in the least amount of timespans possible

  Scenario: A course with tutors who have overlapping times should display the sum of their times
    # Given an empty database
    Given I have a tutor named Michael Jackson who tutors CSE 1341 on Mon 3-4pm
    And I have a tutor named Elvis Presley who tutors CSE 1341 on Mon 4-5pm
    When I log in as a manager
    And I go to the home page
    Then I should see Mon 3-5pm
    And I should not see Mon 3-4pm
    And I should not see Mon 4-5pm

  Scenario: A course with tutors who have non-overlapping times should display the sum of their times
    # Given an empty database
    Given I have a tutor named Michael Jackson who tutors CSE 1341 on Mon 3-4pm
    And I have a tutor named Elvis Presley who tutors CSE 1341 on Mon 4:30-5pm
    When I log in as a manager
    And I go to the home page
    Then I should see 3-4pm
    And I should see 4:30-5pm
    And I should not see 3-5pm