Feature: Look up tutor names for a course
  In order to direct students to an available tutor
  As an ALEC manager or administrator
  I should be able to look up the name of an available tutor for a subject

  Scenario: Manager should be able to log in
    Given a manager account with email "manager@smu.edu" and password "password"
    When I log in with email "manager@smu.edu" and password "password"
    Then I should not see You need to sign in or sign up before continuing
    And I should be on the home page

  Scenario: Manager should not be able to log in with the wrong password
    Given a manager account with email "manager@smu.edu" and password "password"
    When I log in with email "manager@smu.edu" and password "wrong password"
    Then I should see Invalid email or password.
    And I should be on the login page

  Scenario: Manager should be able to view tutor information on the home page
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named George Washington who tutors on Mon 2-3pm, Wed 3-4pm
    When I log in as a manager
    And I go to the home page
    Then I should see George Washington
    And I should see Mon 2-3pm
    And I should see Wed 3-4pm

  Scenario: Manager should be able to view tutor information on a course's page
    Given the course ACCT 2301: Introduction To Financial Accounting with a tutor named George Washington who tutors on Mon 2-3pm, Wed 3-4pm
    When I log in as a manager
    And I go to the home page
    And I click on Introduction To Financial Accounting
    Then I should see George Washington
    And I should see Mon 2-3pm
    And I should see Wed 3-4pm
