# Now

* show tutor room/status on /courses if they are clocked in

* try to update course names from alternate list (from scheduler?)

* tutor statuses
* Update tutor time from the website
* add loading animation when uploading excel

# Immediate future

* room autocomplete
  * maps?
* change where tutors are without clocking them out
* implement status (tutors text in?)
* athlete appointment column - subtract from time they're available
* time system that logs time they were actually there?

# Later

* tutor emails and pictures?
* cach time info in each course (update when their tutor is updated)


add room to clock in
keep track of timesheets (clock in/out)
subscribe to class (text/email you when tutor becomes available)
tutors manage number of students they have (separate logins)
highlight classes with a tutor that's clocked in


# Done

* sort /tutors by time instead of last name (when filtering by day)
* mike (Xinqi) Ren's last name
* expand "1-10pm" under /courses to see list of tutors and when they're there (filtered by who's here now) and where they are
  * shouldn't have to view course
* filter tutors by the day they're here
  * print that list?

* export .csv
* import excel doc directly?
* Ann Shatles: ashattle@smu.edu
  * smu.edu/alec/tutorsched.asp
  * make her a login
  * courses/tutorsched
* upload excel directly (updated)
* export tutor list to excel