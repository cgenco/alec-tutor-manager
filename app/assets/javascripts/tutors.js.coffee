# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$('#star').raty({
  # score : 0,
  target: '#review_rating',
  targetKeep : true,
  targetType : 'number',
  path: "/assets"
})

# $('#filter_day').show()
$('#filter_day input[type=checkbox]').prop('checked', true)

$('#filter_day input').click ->
  console.log "input clicked"
  show = []
  hide = []

  $('#filter_day input[type=checkbox]').each (i) ->
    checkbox = $(this)
    day = '.' + checkbox.attr('name')
    if(checkbox.is(':checked'))
      show.push  day
    else
      hide.push day

  $(hide.join()).hide()
  $(show.join()).show()

$('#today').click (e) ->
  # e.preventDefault()
  console.log "today"
  days = ['.sun', '.mon', '.tue', '.wed', '.thu', '.fri', '.sat']
  today = days[(new Date()).getDay()]
  
  # just show today
  $(days.join()).hide()
  $(today).show()

  # just check the today box
  # TODO: why isn't this actually checking the current day?
  $('#filter_day input[type=checkbox]').prop('checked', false)
  $('#filter_day input[name=' + today.substring(1,4) + ']').prop('checked', true)
