# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$('.tutor_day_breakdown').hide()

$('.course_day_time').click ->
  console.log "click!"
  $(this).children('.tutor_day_breakdown').slideToggle()


days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
today = days[(new Date()).getDay()]
console.log(today)

$('.tutors_by_day').hide()
$('.' + today).show()
$('table.week th').click ->
  console.log "click!"

  # slide down the correct detail list of tutors
  day = $(this).text()
  parent = $(this).parents("td")
  parent.children('.tutors_by_day').slideUp()
  parent.children('.' + day).slideDown()
  # $(this).children('.tutor_day_breakdown').slideToggle()

  # highlight this th
  parent.find('.today').removeClass('today')
  $(this).addClass('today')