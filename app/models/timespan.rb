class Timespan
  attr_accessor :start_time, :stop_time

  def initialize(params = {})
    params[:start_time] ||= nil
    params[:stop_time] ||= nil

    @start_time = params[:start_time]
    @stop_time = params[:stop_time]
  end

  # NOTE: this won't work for times that don't pass an AM/PM marker
  def self.parse(str)
    return nil unless str && !str.empty?

    # "M2-5pm", "Monday 2-5p", "MON 2-5"
    str = str.strip.upcase

    # get the letter portion of the string
    str =~ /^\s*([A-Z]+)/
    day = $1
    
    # get am/pm if it's there
    str =~ /([A-Z]+)\s*$/
    apm = $1
    apm = (apm.index("A") ? 'AM' : 'PM') if apm

    # remove day and am/pm from the string
    str.tr!(apm, '') if apm
    str.tr!(day, '') if day

    start, stop = str.split(/\-/)

    start_time = DateTime.parse("#{day} #{start} #{apm}")
    stop_time = DateTime.parse("#{day} #{stop} #{apm}")

    # make sure start_time comes before stop_time

    start_time, stop_time = [start_time, stop_time].sort
    
    # print "returning the timespan: "
    # p t
    Timespan.new(start_time: start_time, stop_time: stop_time)
  end

  # parse 'M 2-5p'
  # parse "Sun 7:30-10pm"

  # returns the abbreviated day of the week
  def day
    return nil unless @start_time

    @start_time.strftime "%a"
  end

  def span
    return nil unless @start_time && @stop_time

    "#{time_string @start_time}-#{time_string @stop_time}#{@stop_time.strftime('%P')}"
  end

  def +(timespan)
    timespans = [self, timespan].sort

    # if the start time of the earlier time is on or after
    # the end time of the later time,
    # combine them.
    if timespans[0].stop_time >= timespans[1].start_time
      start = timespans[0].start_time
      stop = [timespans[0].stop_time, timespans[1].stop_time].sort.last

      timespans = [Timespan.new(start_time: start, stop_time: stop)]
    end

    timespans
  end

  # compare their first times
  def <=>(timespan)
    # sort this timespan over the other one if the other one is nil
    return -1 unless timespan && timespan.start_time
    return 1 unless @start_time
    @start_time <=> timespan.start_time
  end

  def ==(timespan)
    @start_time == timespan.start_time && @stop_time == timespan.stop_time 
  end

  def self.by_day(timespans)
    # split list of timespans by day
    days = {}
    timespans.each{|timespan|
      day = timespan.start_time.strftime("%w").to_i
      days[day] ||= []

      days[day] << timespan
    }
    Hash[days.sort]
  end

  def self.reduce(timespans)
    days = self.by_day(timespans)

    # try to consolidate each day
    days.each{|day, timespans|
      timespans.sort!
      reduced_timespans = [timespans.first]
      timespans[1..-1].each{ |timespan|
        # reduced_timespans.flatten!
        # try to add this timespan with the latest reduced timespan
        reduced = reduced_timespans.last + timespan
        if reduced.size == 1
          reduced_timespans[-1] = reduced.first
        else
          # else, this timespan deserves to be alone
          reduced_timespans << timespan
        end
      }
      days[day] = reduced_timespans
    }

    # schedule_start = @tutors.map(&:schedule_start).min
    # schedule_end = @tutors.map(&:schedule_end).max
    # time_string(schedule_start) + "-" + time_string(schedule_end) + schedule_end.strftime("%P")
    days.values.flatten
  end

  def is_today?
    @start_time.strftime("%a") == Date.today.strftime("%a")
  end

  def is_now?
    now = DateTime.now
    # now = DateTime.parse('4pm')
    is_today? && 
      @start_time.hour*60 + @start_time.minute <= now.hour*60 + now.minute && 
      @stop_time.hour*60 + @stop_time.minute >= now.hour*60 + now.minute
  end

  def to_s
    "#{day} #{span}"
  end

  private

  def time_string(time)
    zero_minutes = time.min == 0
    time.strftime("%l" + (zero_minutes ? '' : ":%M")).strip
  end
end