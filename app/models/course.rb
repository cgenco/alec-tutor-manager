class Course < ActiveRecord::Base
  attr_accessible :name, :course_number
  has_and_belongs_to_many :tutors
  after_create :generate_course_number

  # CACHE THIS
  def scheduled_time
    @tutors ||= tutors
    return nil if @tutors.empty?

    # get all the times for this course
    Timespan.reduce(@tutors.map(&:schedule).map(&:values).flatten)
  end

  def department
    course_number.split(" ").first if course_number?
  end

  def total_available_spots
    @tutors ||= tutors

  end

  def currently_tutoring
    @tutors ||= tutors
    return 0 if @tutors.empty?

    @tutors.map(&:currently_tutoring).sum
  end

  def capacity
    @tutors ||= tutors
    return 0 if @tutors.empty?

    @tutors.map(&:capacity).sum
  end

  # TODO: delete this method
  def generate_course_number
    update_attribute(:course_number, name.split(":").first) if name && name.index(':') && course_number.nil?
  end

  def to_s
    name ? "#{course_number}: #{name}" : course_number
  end

  # Class methods
  def self.import(file)
    require 'json'

    courses = []

    JSON.parse(File.read(file.path)).each do |c|
      course = {}
      course[:name] = c["Title"].titlecase
      course[:course_number] = c["Course"]
      courses << course
    end

    # now we have a `courses` hash with the important data
    # time to perminantly update the records


    courses.each{|course_data|
      course = Course.find_or_create_by_course_number(course_data[:course_number])
      p course
      p course_data
      course.update_attributes(course_data)
    }
  end

  private

  def time_string(time)
    zero_minutes = time.min == 0
    time.strftime("%l" + (zero_minutes ? '' : ":%M")).strip
  end
end
