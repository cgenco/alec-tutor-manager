class Review < ActiveRecord::Base
  attr_accessible :email, :rating, :review, :subject, :tutor_name
end
