require_dependency 'timespan'

class Tutor < ActiveRecord::Base
  attr_accessible :status, :email, :name, :room, :checked_in, :course_ids, :schedule, :course_numbers, :timespans
  # attr_accessor :course_numbers
  validates_presence_of :name

  # {0: [{start: Time, stop: Time}, {start: Time, stop: Time}], 1: ['1-2']}
  serialize :schedule, Hash

  has_and_belongs_to_many :courses
  after_initialize :assign_default_values

  # used as shortcut when creating records
  def course_numbers=(course_nums)    
    # remove all current course associations
    courses.delete_all

    course_nums.each{|course_num|
      course = Course.find_or_create_by_course_number(course_num.strip)
      courses << course
    }
    save
  end

  # for form helper
  def timespans
    schedule.values.join(', ')
  end

  # parses an array like:
  # ["M 1-2", "W 3-4"]
  def timespans=(timespans)
    timespans = timespans.split(',').map(&:strip) if timespans.class == String
    
    timespans.map!{|t|
      Timespan.parse(t)
    }
    self.schedule = Timespan.by_day(timespans)
  end

  def names
    name.split(' ')
  end

  def is_available?
    checked_in
  end

  def tutors_on?(day)
    day_code = (day.to_i == day ? day : DateTime.parse(day.to_s).wday)
    !schedule[day_code].nil?
  end

  def timespan_today
    schedule[Date.today.wday]
  end

  def full_schedule
    schedule.values.flatten.join ", "
  end

  # def should_be_here?
    # now = Time.now
    # schedule_start < now && now < schedule_end
  # end

  def self.to_csv
    CSV.generate do |csv|
      columns = %w(name schedule courses)
      csv << columns
      all.each do |tutor|
        # csv << tutor.attributes.values_at(*column_names)
        values = [tutor.name]
        values << tutor.schedule.values.join(", ")
        values << tutor.courses.join(", ")
        csv << values
      end
    end
  end

  def self.import(file)
    keys = nil
    students = {}

    spreadsheet = open_spreadsheet(file)
    # binding.pry
    # use the "ALL" sheet
    spreadsheet.default_sheet = spreadsheet.sheets.find{|sheet| /website/i =~ sheet}

    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      course = Hash[[header, spreadsheet.row(i)].transpose]
      student = {}
      student[:name] = "#{course['First Name']} #{course['Last Name']}"

      # get the schedule
      student[:schedule] = {}
      %w(Sunday Monday Tuesday Wednesday Thursday Friday).each_with_index{|day, i|
        span = course[day]
        student[:schedule][i] = Timespan.parse("#{day} #{span}") if span
      }

      # get the course numbers
      student[:course_numbers] = []
      
      # save or find the temporary record
      students[student[:name]] ||= student

      # add the course
      # excel adds a decimal to numbers?
      course_num = course['Course Number'].to_s.gsub(".0", '')
      course_num = "#{course['Course']} #{course_num}"
      students[student[:name]][:course_numbers] << course_num
    end
    # puts students.to_yaml
    # raise 'lol'

    # now we have a `students` hash with all our data
    # time to perminantly update the records

    students.each{|name, student_data|
      student = Tutor.find_or_create_by_name(name)
      student.update_attributes(student_data)
    }
  end

  private

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Csv.new(file.path, nil, :ignore)
    when ".xls" then Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def assign_default_values  
    self.room ||= %w(202N 202R 202K 202U 202T 202V CONFERENCE).sample

    # start 2-7pm 
    # self.schedule_start ||= Time.parse("#{2 + rand(3)}pm")
    # self.schedule_end ||= self.schedule_start + (2 + rand(4)).hours
  end
end
