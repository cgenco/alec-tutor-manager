class AddCheckedInToTutors < ActiveRecord::Migration
  def change
    add_column :tutors, :checked_in, :boolean
  end
end
