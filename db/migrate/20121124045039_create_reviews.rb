class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :tutor_name
      t.string :subject
      t.integer :rating
      t.text :review
      t.string :email

      t.timestamps
    end
  end
end
