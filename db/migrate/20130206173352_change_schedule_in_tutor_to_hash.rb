class ChangeScheduleInTutorToHash < ActiveRecord::Migration
  def up
    add_column :tutors, :schedule, :text
    remove_column :tutors, :schedule_start
    remove_column :tutors, :schedule_end
  end

  def down
    remove_column :tutors, :schedule, :text
    add_column :tutors, :schedule_start, :time
    add_column :tutors, :schedule_end, :time
  end
end
