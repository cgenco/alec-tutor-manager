class AddStatusToTutor < ActiveRecord::Migration
  def change
    add_column :tutors, :status, :string
  end
end
