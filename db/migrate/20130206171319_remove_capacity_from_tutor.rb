class RemoveCapacityFromTutor < ActiveRecord::Migration
  def up
    remove_column :tutors, :capacity
  end

  def down
    add_column :tutors, :capacity, :integer
  end
end
