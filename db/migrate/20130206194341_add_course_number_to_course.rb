class AddCourseNumberToCourse < ActiveRecord::Migration
  def up
    add_column :courses, :course_number, :string
    Course.all.each{|c| c.generate_course_number}
  end

  def down
    remove_column :courses, :course_number
  end
end
