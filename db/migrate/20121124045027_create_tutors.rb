class CreateTutors < ActiveRecord::Migration
  def change
    create_table :tutors do |t|
      t.string :name
      t.time :schedule_start
      t.time :schedule_end
      t.integer :capacity
      t.integer :currently_tutoring
      t.string :room
      t.string :email

      t.timestamps
    end
  end
end
