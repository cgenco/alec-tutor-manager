class CreateTableCoursesTutors < ActiveRecord::Migration
  def up
    create_table "courses_tutors", :id => false do |t|
      t.column "course_id", :integer, :null => false
      t.column "tutor_id",  :integer, :null => false
    end
  end

  def down
    drop_table "courses_tutors"
  end
end
