class RemoveCurrentlyTutoringFromTutor < ActiveRecord::Migration
  def up
    remove_column :tutors, :currently_tutoring
  end

  def down
    add_column :tutors, :currently_tutoring, :integer
  end
end
