# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(email: "manager@smu.edu", password: 'password')

acct = Course.create!(name: "Introduction To Financial Accounting", course_number: "ACCT 2301")
cse = Course.create!(course_number: "CSE 1341")
biol = Course.create!(course_number: "BIOL 1401")
econ = Course.create!(course_number: "ECON 1401")
emis = Course.create!(course_number: "EMIS 1334")



george = Tutor.create!(name: "George Washington", course_numbers: [acct.course_number], timespans: "Mon 2-3pm, Wed 3-4pm".split(','))

paul = Tutor.create!(name: "Paul Newman", course_numbers: [acct.course_number], timespans: "Mon 2-3pm, Wed 3-4pm".split(','))

joe = Tutor.create!(name: "Joe Blogs", course_numbers: [cse].map(&:course_number), timespans: "Mon 2-3pm, Wed 3-4pm".split(','))

michael = Tutor.create!(name: "Michael Jackson", course_numbers: [cse, biol].map(&:course_number), timespans: "Mon 3-4pm, Tue 5-7pm".split(','))

elvis = Tutor.create!(name: "Elvis Presley", course_numbers: [cse].map(&:course_number), timespans: "Mon 4-5pm".split(','))

kanye = Tutor.create!(name: "Kanye West", course_numbers: [cse, econ, emis].map(&:course_number), timespans: "Thu 12-8pm, Fri 5-9pm".split(','))
