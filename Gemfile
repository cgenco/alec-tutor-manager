source 'https://rubygems.org'

gem 'rails', '3.2.11'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

group :test do
  # for application-level testing
  # https://github.com/cucumber/cucumber-rails
  gem 'cucumber-rails', :require => false

  # needed for cucumber
  gem 'database_cleaner'

  # for scripting browsers?
  # recommended by http://railscasts.com/episodes/155-beginning-with-cucumber
  # https://github.com/brynary/webrat
  gem 'webrat'
end

group :test, :development do
  gem 'rspec-rails', "~> 2.0"
  # from https://github.com/guard/guard-rspec
  gem 'guard-rspec'

  # for diagrams
  # https://github.com/preston/railroady
  # rake diagram:all
  gem 'railroady'

  # for diagrams
  # http://rails-erd.rubyforge.org/install.html
  # rake erd
  gem "rails-erd"
end

group :development do
  gem 'sqlite3'
end

group :production do
  # if failed extensions, run this first:
  # sudo aptitude install libpq-dev
  gem 'pg'
  gem 'thin'
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end

# to import excel documents
# from http://railscasts.com/episodes/396-importing-csv-and-excel
# using older version to fix name error: http://www.ruby-forum.com/topic/4411090
gem 'roo', '1.9.5'
# to fix zip error: http://stackoverflow.com/questions/5997539/using-rubyzip-error-no-such-file-to-load-zip-zip
gem 'rubyzip', :require => 'zip/zip'

gem "therubyracer"
gem "less-rails"
gem "twitter-bootstrap-rails" #, :git => 'git://github.com/seyhunak/twitter-bootstrap-rails.git', :branch => 'static'

gem 'jquery-rails'
gem 'fancybox2-rails', '~> 0.2.1'

gem 'haml'

gem 'sass'


# https://github.com/plataformatec/devise
gem 'devise'

# doesn't work?
# gem 'chosen-rails'

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'

gem 'faker'

# if native extensions fail when installing nokogiri, run:
# sudo apt-get install libxslt-dev libxml2-dev
